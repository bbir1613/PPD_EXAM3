package domain;

public class Sistem {
    private int[] b;
    int[][] a;

    public Sistem() {
    }
    public Sistem(int[][] a, int[] b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a[0].length; ++i) {
            for (int j = 0; j < a[0].length; ++j) {
                sb.append(String.format(" %d*x[%d]", a[i][j], j));
            }
            sb.append(String.format(" = %d \n", b[i]));
        }
        return sb.toString();
    }
}
